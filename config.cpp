#include <string>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace bfs = boost::filesystem;
namespace bpt = boost::property_tree;

#include "buildvars.hpp"

#include "config.hpp"

Config::Config(const bfs::path& config_path) {
  if(config_path.is_absolute()) {
    path = config_path;
  }
  else if(BuildVars::sysconfdir.is_absolute()) {
    path = BuildVars::sysconfdir / config_path;
  }
  else {
    path = BuildVars::install_prefix / BuildVars::sysconfdir / config_path;
  }
  
  bpt::read_json(path.string(), vars);
  return;
}

Config::Config(const std::string& config_path) : Config::Config(bfs::path(config_path)) {
}

std::string Config::get(const std::string& key) const {
  return vars.get<std::string>(key);
}
