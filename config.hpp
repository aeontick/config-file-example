/* Dependencies:
#include <string>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>

namespace bfs = boost::filesystem;
namespace bpt = boost::property_tree;
*/

class Config {
public:
  Config(const bfs::path& config_path);
  Config(const std::string& config_path);
  std::string get(const std::string& key) const;
private:
  bpt::ptree vars;
  bfs::path path;
};
