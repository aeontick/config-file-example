#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace bfs = boost::filesystem;
namespace bpt = boost::property_tree;

#include "buildvars.hpp"
#include "config.hpp"

int main() {
  Config example = BuildVars::config_filename;
  std::cout << "Tutorial last updated by " << example.get("author") << " in the year " << example.get("year") << ".\n";
}
